import Modal from "../Modal";
import ModalClose from "../ModalClose/ModalClose";
import ModalHeader from "../ModalHeader/ModalHeader ";
import ModalBody from "../ModalBody/ModalBody";
import ModalFooter from "../ModalFooter/ModalFooter";

import PropTypes from "prop-types";

function ModalImage({ onClose, onConfirm, product }) {
  return (
    <Modal onClose={onClose}>
      <ModalClose onClick={onClose} />
      <ModalHeader>
        <img
          src={product?.ImageUrl}
          style={{ height: "200px" }}
          alt="product"
        />
      </ModalHeader>
      <ModalBody>
        <h2>PRODUCT DELETE</h2>
        <p>
          By clicling the "Yes, Delete" button, {product?.Name} will be deleted
        </p>
      </ModalBody>
      <ModalFooter
        firstText="NO, CANCEL"
        secondaryText="YES, DELETE"
        firstClick={onClose}
        secondaryClick={onConfirm}
      />
    </Modal>
  );
}

ModalImage.propTypes = {
  product: PropTypes.shape({
    Name: PropTypes.string.isRequired,
    Price: PropTypes.number.isRequired,
    ImageUrl: PropTypes.string.isRequired,
    SKU: PropTypes.number.isRequired,
    Color: PropTypes.string,
  }),
  onClose: PropTypes.func,
  onConfirm: PropTypes.func,
};

export default ModalImage;
