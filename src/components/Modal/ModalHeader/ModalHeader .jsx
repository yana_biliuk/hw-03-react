import styled from "styled-components";
import PropTypes from "prop-types";

const TextHeader = styled.div`
  display: flex;
  justify-content: center;
  font-size: 32px;
  font-weight: 400;
`;

function ModalHeader({ children }) {
  return <TextHeader>{children}</TextHeader>;
}
ModalHeader.propTypes = {
  children: PropTypes.node.isRequired,
};
export default ModalHeader;
