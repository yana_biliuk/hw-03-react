import { useOutletContext } from "react-router-dom";
import styled from "styled-components";
import ModalImage from "../components/Modal/ModalImage/ModalImage";
import { useState } from "react";
import ProductCard from "../components/ProductCard/ProductCar";

const CartContainer = styled.div`
  padding: 20px;
  max-width: 1200px;
  margin: 0 auto;
`;

const CartHeader = styled.h1`
 text-align: center;
  margin-bottom: 40px;
  // font-size: 28px;
  // text-align: center;
  // font-weight: bold;
  // margin-bottom: 20px;
`;

const CartEmpty = styled.p`
  font-size: 18px;
  text-align: center;
  color: #888;
`;

const CartItems = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 20px;
`;

function CartPage() {
  const { cart, handleAddToCart, handleToggleFavorite, favorites, setCart } =
    useOutletContext();
  const [productToRemove, setProductToRemove] = useState(null);

  const handleRemoveClick = (product) => {
    setProductToRemove(product);
  };

  const handleConfirmRemove = () => {
    setCart(cart.filter((item) => item.SKU !== productToRemove.SKU));
    setProductToRemove(null);
  };

  const handleCloseModal = () => {
    setProductToRemove(null);
  };

  return (
    <CartContainer>
      <CartHeader>Your cart</CartHeader>
      {cart.length === 0 ? (
        <CartEmpty>Your cart is empty</CartEmpty>
      ) : (
        <CartItems>
          {cart.map((product, index) => (
            <ProductCard
              key={index}
              product={product}
              handleAddToCart={handleAddToCart}
              handleToggleFavorite={handleToggleFavorite}
              handleRemoveFromCart={handleRemoveClick}
              showRemoveIcon={true}
              favorites={favorites}
              showButton={false}
              cart={cart}
            />
          ))}
        </CartItems>
      )}

      {productToRemove && (
        <ModalImage
          onClose={handleCloseModal}
          onConfirm={handleConfirmRemove}
          product={productToRemove}
        />
      )}
    </CartContainer>
  );
}

export default CartPage;
