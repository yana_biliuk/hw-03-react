import { useOutletContext } from "react-router-dom";
import styled from "styled-components";
import ProductList from "../components/ProductList/ProductList";

const FavoritesContainer = styled.div`
  padding: 20px;
  max-width: 1200px;
  margin: 0 auto;
`;
const Title = styled.h1`
  text-align: center;
  margin-bottom: 40px;
`;

const FavoritesEmpty = styled.p`
  text-align: center;
  margin-bottom: 40px;
  font-size: 18px;
  color: #888;
`;

function FavoritePaga() {
  const { products, handleAddToCart, handleToggleFavorite, favorites,cart } = useOutletContext();

  const favoriteProducts = products.filter((product) => favorites[product.SKU]);

  return (
    <FavoritesContainer>
      <Title>Favorite products</Title>
      {favoriteProducts.length === 0 ? (
        <FavoritesEmpty>
          You have not added any products to your favorites
        </FavoritesEmpty>
      ) : (
        <ProductList
          products={favoriteProducts}
          handleAddToCart={handleAddToCart}
          handleToggleFavorite={handleToggleFavorite}
          favorites={favorites}
          showButton={true}
          cart={cart}
        />
      )}
    </FavoritesContainer>
  );
}

export default FavoritePaga;
