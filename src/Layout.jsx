import { Outlet, Link } from "react-router-dom";
import Header from "./components/Header/Header";
import ModalText from "./components/Modal/ModalText/ModalText";
import { useEffect, useState } from "react";

import styled from "styled-components";

const MenuItem = styled(Link)`
  display: inline-block;
  text-decoration: none;
  font-size: 24px;
  padding: 20px 30px;
  color: black;
  &: hover {
    color: darkviolet;
    cursor: pointer;
  }
`;

function Loyout() {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [products, setProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const storedFavorites = localStorage.getItem("favorites");
    const storedCart = localStorage.getItem("cart");

    if (storedFavorites) {
      setFavorites(JSON.parse(storedFavorites));
    }

    if (storedCart) {
      setCart(JSON.parse(storedCart));
    }
  }, []);

  useEffect(() => {
    fetch("/data.json")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => setProducts(data))
      .catch((error) => console.error("Error fetching products:", error));
  }, []);

  // Оновлюємо локальне сховище при зміні кошика
  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  // Оновлюємо локальне сховище при зміні товарів
  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorites));
  }, [favorites]);

  const handleAddToCart = (product) => {
    if(!cart.find (item => item.SKU === product.SKU)){
  
    setSelectedProduct(product);
    setIsModalOpen(true);
  }}


  const handleConfirmAddToCart =()=>{
    if (selectedProduct && !cart.find(item=>item.SKU === selectedProduct.SKU)){
      const updatedCart = [...cart, selectedProduct];
    setCart(updatedCart);
    setIsModalOpen(false);
    setSelectedProduct(null)

    }
  }
    
  

  const handleToggleFavorite = (productSKU) => {
    const updatedFavorites = {
      ...favorites,

      [productSKU]: !favorites[productSKU],
    };
    setFavorites(updatedFavorites);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
    setSelectedProduct(null);
  };

  const cartCount = cart.length;
  const favoritesCount = Object.values(favorites).filter(
    (favorite) => favorite
  ).length;

  return (
    <>
      <Header
        isFavoriteCart={cartCount > 0}
        isFavoriteFavorites={favoritesCount > 0}
        cartCount={cartCount}
        favoritesCount={favoritesCount}
      >
        <div>
          <MenuItem className="menu__item" to="/">
            HomePage
          </MenuItem>
          <MenuItem to="/cart">Cart</MenuItem>
          <MenuItem to="/favorite">Favoriters</MenuItem>
        </div>
      </Header>
      <Outlet
        context={{
          products,
          handleAddToCart,
          handleToggleFavorite,
          favorites,
          isModalOpen,
          selectedProduct,
          handleCloseModal,
          cart,
          setCart,
        }}
      />
      {isModalOpen && (
        <ModalText onClose={handleCloseModal} product={selectedProduct} onComfirm={handleConfirmAddToCart} />
      )}
    </>
  );
}

export default Loyout;
